import React from 'react'


export default props => {

    //props.test for verdadeiro retorna o props.children que é o objeto que está dentro da tag IF se não retorna false
    if(props.test){

        return props.children
    } else {

        return false
    }
}