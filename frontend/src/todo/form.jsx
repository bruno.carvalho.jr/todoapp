import React from 'react'
import Grid from '../template/grid'
import Button from '../template/iconButton'

export default props => {

    const keyhandler = (e) => {
        if(e.key === 'Enter') {
            e.shiftKey ? props.handleSearch() : props.handleAdd()
        } else if(e.key === 'Escape') {
            props.handleClear()
        }
    }

    return (

        <div role='form' className='form'>
    
            <Grid cols='12 9 10'>
                <input id='description' className='form-control' 
                    placeholder='Adicione uma tarefa'
                    value={props.description}
                    onChange={props.handleChange}
                    onKeyUp={keyhandler}
                    >
                        
                </input>
            </Grid>
    
            <Grid cols='12 3 2'>
                <Button style='primary' icon='plus'
                    onClick={props.handleAdd} />
                <Button style='info' icon='search'
                    onClick={props.handleSearch} />
                <Button style='default' icon='close'
                    onClick={props.handleClear} />
            </Grid>
            
        </div>
    )
}